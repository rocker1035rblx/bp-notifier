const Discord = require('discord.js')

const { notifier, checkForNewItems } = require('./notifier')

require('dotenv').config()

const notifyHook = new Discord.WebhookClient(
    process.env.HOOK_USER_ID, 
    process.env.HOOK_TOKEN
)

function returnItemType(item) {
    let str = `New ${item.ItemType}`

    if (item.IsUnique)
        str = `New Unique ${item.ItemType}`

    if (!item.IsOnSale)
        str = `New Off-sale ${item.ItemType}`

    return '**' + str + '**'
}

function generateEmbed(item) {
    const embed = new Discord.RichEmbed()
        .setColor(16777215)
        .setTitle(`https://www.brickplanet.com/store/${item.ID}/`)
        .attachFile( {attachment: './ui/bpicon.png'} )
        .setAuthor(item.Name, 'attachment://bpicon.png', `https://www.brickplanet.com/store/${item.ID}/`)
        .setThumbnail('https://cdn.brickplanet.com/' + item.Image)

    if (item.IsUnique)
        embed.setColor(35308)

    if (!item.IsOnSale)
        embed.setColor(5131854)

    if (item.PriceCredits > 0)
    embed.addField('Credits', item.PriceCredits.toLocaleString(), true)

    if (item.PriceBits > 0)
    embed.addField('Bits', item.PriceBits.toLocaleString(), true)

    return embed
}

notifier.on('newItem', (item) => {
    notifyHook.send(returnItemType(item) + ' @everyone', generateEmbed(item))
        .catch((err) => {
            console.log('Failure notifying item: ' + item.Name)
            console.error(err.stack)
        })
})

setInterval(checkForNewItems, 1000 * 10)

console.log('Notifier is now started.')