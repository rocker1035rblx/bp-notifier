const EventEmitter = require('events')
const getRecentItems = require('./api/getRecentItems')

const notifier = new EventEmitter()

let cachedItems = []

async function checkForNewItems() {
    const items = await getRecentItems()

    // Fill cached items if empty, or reset it if it gets too big
    if (!cachedItems.length || cachedItems.length > 150) {
        cachedItems = []
        items.forEach((item) => cachedItems.push(item.ID))
    }

    items.forEach((item) => {
        if (!cachedItems.includes(item.ID)) {
            cachedItems.push(item.ID)
            console.log(`New item : https://www.brickplanet.com/store/${item.ID}/`)
            notifier.emit('newItem', item)
        }
    })
}

module.exports = { notifier, checkForNewItems }