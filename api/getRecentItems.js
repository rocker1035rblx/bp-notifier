let p = require('phin')

const ITEM_API = 'https://www.brickplanet.com/web-api/store/get-recent-items'

async function getRecentItems() {
    try {
       const res = await p({
            'url': ITEM_API,
            'parse': 'json'
        })
        return res.body
    } catch (err) {
        return {}
    }
}

module.exports = getRecentItems